'''

    name:   Alen Arias Vazquez
    mail:   alen.arias.vazquez@cern.ch
    group:  BE-CEM-EDL
    date:   22/03/2022
    Control CP400DP using basic HTML server

'''

import sys
import serial
import argparse
import socket
import logging, logging.config

from string import Template
from http.server import BaseHTTPRequestHandler, HTTPServer

sys.path.insert(0, '../')

from cpx400dp import CPX400DP

for i in range(2):
    try:
        psu = CPX400DP(devname='/dev/ttyACM'+str(i), baudrate=115200)
        break
    except serial.SerialException:
        if i == 1:
            exit()

'''
    Class for HTTP Methods

'''
class HTTPWebServer(BaseHTTPRequestHandler):

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_REDIRECT(self, path):
        self.send_response(303)
        self.send_header('Content-type', 'text/html')
        self.send_header('Location', path)
        self.end_headers()

    def do_GET(self):
        f = open("HTML/web_server.html", "r")
        html_page = f.read()
        html_page = self.update_status(html_page)
        self.do_HEAD()
        self.wfile.write(html_page.encode("utf-8"))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode("utf-8")
        name = post_data.split("=")[0]
        value = post_data.split("=")[1]
        self.processing_post_data(name=name, value=value)
        self.do_REDIRECT('/')

    def processing_post_data(self,name, value):
        if name == "SW1":
            if value=="ON" and psu.CHAN_STATUS[0]==False:
                psu.turn_on_chan(chan=1)
            elif value=="OFF" and psu.CHAN_STATUS[0]==True:
                psu.turn_off_chan(chan=1)
        elif name == "SW2":
            if value=="ON" and psu.CHAN_STATUS[0]==False:
                psu.turn_on_chan(chan=2)
            elif value=="OFF" and psu.CHAN_STATUS[1]==True:
                psu.turn_off_chan(chan=2)
        elif name == "V1":
            psu.set_voltage(volts=value, chan=1)
        elif name == "V2":
            psu.set_voltage(volts=value, chan=2)
        elif name == "I1":
            psu.set_current(amps=value, chan=1)
        elif name == "I2":
            psu.set_current(amps=value, chan=2)

    def update_status(self,html):
        if psu.CHAN_STATUS[0] == True:
            html=Template(html).safe_substitute(S1="ON")
        else:
            html=Template(html).safe_substitute(S1="OFF")
        if psu.CHAN_STATUS[1] == True:
            html=Template(html).safe_substitute(S2="ON")
        else:
            html=Template(html).safe_substitute(S2="OFF")
        return html

'''
    Get your own IP
'''
def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]
    except socket.error:
        print("Caught exception socket.error : {}".format(socket.error))
        return "localhost"

'''
    MAIN
'''
def main():
    parser = argparse.ArgumentParser(description='Basic Web Application')
    parser.add_argument('--port', '-p', type=int, default= 8080, help='Port for the server')
    args = parser.parse_args()

    ip_addr = get_ip_address()

    MyWebServer=HTTPServer((ip_addr, args.port), HTTPWebServer)
    print("Server URL http://{}:{}".format(ip_addr, args.port))

    try:
        MyWebServer.serve_forever()
    except KeyboardInterrupt:
        MyWebServer.server_close()
        psu.close()
        print("Server stopped by the user")

if __name__ == '__main__':
    sys.exit(main())
