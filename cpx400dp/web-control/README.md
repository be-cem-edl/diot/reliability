# CP400DP Web Control

![](web.png)

## Description

Python class to control remotely the power bia web server in HTML

## Usage
```bash
python3 web_server.py
```

## How to install service
```bash
sudo cp <root_dir>/cpx400dp.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo start cpx400dp.service
```