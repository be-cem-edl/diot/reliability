'''

    name:   Alen Arias Vazquez
    mail:   alen.arias.vazquez@cern.ch
    group:  BE-CEM-EDL
    date:   27/09/2022
    Logging CP400DP from UART

'''

import sys
import os
import time
import multiprocessing
import logging, logging.config
import argparse

from cpx400dp import CPX400DP

class CPX400DPLog(object):

    def __init__(self, dirname='dirname', logname='CPX400DPLog', log_level=logging.INFO):
        logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)s-%(levelname)-s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        self.logname = logname
        self.log = logging.getLogger(self.logname)
        self.dirname = dirname
        self.ext='.csv'
        self.psu=CPX400DP()

    def create_directory(self):
        if not os.path.exists(self.dirname):
            os.makedirs(self.dirname)

    def get_file_name(self, filename, number=0):
        found = False
        while (found==False):
            if not os.path.exists(self.dirname+'/'+filename+'_'+str(number)+self.ext):
                found = True
            else:
                number=number+1
        return self.dirname+'/'+filename+'_'+str(number)+self.ext

    def get_data(self, interval=1.0):
        self.create_directory()
        log_file_name=self.get_file_name(filename='psu_logging',number=0)
        log_file=open(log_file_name, 'w+')
        log_file.write("TIME,V1,I1,V2,I2\n")
        while True:
            V1, V2, I1, I2 = self.psu.get_parameters()
            log_file.write("{},{},{},{},{}\n".format(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.time())),V1,I1,V2,I2))
            log_file.close()
            time.sleep(interval)
            log_file=open(log_file_name, 'a+')

def main():
    parser = argparse.ArgumentParser(description='CPX400DP logging', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dir',   "-d",   type=str,     default='log_cpx400dp', help="Directory to storage results")
    parser.add_argument('--time',  "-t",   type=float,   default=1.0           , help="Logging interval in seconds")
    args = parser.parse_args()

    test = CPX400DPLog(dirname=args.dir)
    try:
        test.get_data(interval=args.time)
    except KeyboardInterrupt:
        test.psu.close()

if __name__ == '__main__':
    sys.exit(main())