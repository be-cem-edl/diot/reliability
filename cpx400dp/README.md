# CPX400DP

![](cpx400dp.jpg)

python class to control remotely the power supply bia USB

## Usage

- Give rights to the device
```bash
sudo chmox a+rx /dev/ttyACM0
```
```bash
crw-rw-rw- 1 root plugdev 166, 0 Mar 23 09:46 /dev/ttyACM0
```
```bash
python3 cpx400dp -h
```
the help menu is shown below:

```bash
usage: cpx400dp.py [-h] [--device DEVICE] [--baudrate BAUDRATE] [--chan CHAN] [--volts VOLTS] [--amps AMPS] [--powerup]

Control CPX400DP from USB

optional arguments:
  -h, --help            show this help message and exit
  --device DEVICE, -D DEVICE
                        Linux device name (default: /dev/tty_CPX)
  --baudrate BAUDRATE, -b BAUDRATE
                        Serial baudrate (default: 115200)
  --chan CHAN, -C CHAN  Channel selector (default: 1)
  --volts VOLTS, -V VOLTS
                        Volts (default: 12.0)
  --amps AMPS, -A AMPS  Amps (default: 0.2)
  --powerup             Switch On requested channel with requested values (default: False)
```

## Configure udev rule

- Create file with udev rule
```bash
touch /etc/udev/rules.d/71-CPX400DP.rules
```
- File the file with the line below:
```bash
SUBSYSTEMS=="usb", ATTRS{idVendor}=="103e", ATTRS{idProduct}=="0460", GROUP="plugdev", MODE="0666", SYMLINK+="tty_CPX"
```
- Restart udev:
```bash
sudo udevadm control --reload-rules && sudo service udev restart && sudo udevadm trigger
```
```bash
$ ls -l /dev/ttyACM0
lrwxrwxrwx 1 root root 7 Mar 23 09:04 /dev/tty_CPX -> ttyACM0
```
