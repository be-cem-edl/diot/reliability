'''

    name:   Alen Arias Vazquez
    mail:   alen.arias.vazquez@cern.ch
    group:  BE-CEM-EDL
    date:   22/03/2022
    Control CP400DP by UART

'''

import sys
import serial
import logging, logging.config
import argparse

class CPX400DP(object):

    STATUS = {
                0x0 : 'OFF',
                0x1 : 'ON'
            }

    def __init__(self, name='CPX400DP', devname='/dev/ttyACM0', baudrate=115200, log_level=logging.INFO):
        logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)s-%(levelname)-s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        self.name = name
        self.log = logging.getLogger(self.name)
        self.CHAN_STATUS=[False, False]
        try:
            self.serial=serial.Serial(devname, baudrate)
            self.log.info("Opening device {}".format(self.serial.name))
        except serial.SerialException as msg:
            self.log.error("{}".format(msg))
            raise serial.SerialException

    def __del__(self):
        self.log.info("Deleting class {}".format(self.__class__.__name__))

    def close(self):
        self.log.info("Closing device {}".format(self.serial.name))
        self.serial.close()

    def send_cmd(self, cmd):
        self.serial.write(bytes(cmd.encode('utf-8')))

    def recv_cmd(self):
        return self.serial.readline()

    def set_voltage(self, volts, chan=1):
        self.log.info("Set Voltage {}V in channel {}".format(volts,chan))
        cmd = 'V'+str(chan)+' '+str(volts)+'\n'
        self.send_cmd(cmd)

    def get_voltage(self, chan=1):
        cmd = 'V'+str(chan)+'O?\n'
        self.send_cmd(cmd)
        resp = self.recv_cmd()
        self.log.info("Voltage channel {}: {}V".format(chan,float(resp[:-3])))
        return float(resp[:-3])

    def set_current(self, amps, chan=1):
        self.log.info("Set current to {}A in channel {}".format(amps,chan))
        cmd ='I'+str(chan)+' '+str(amps)+'\n'
        self.send_cmd(cmd)

    def get_current(self, chan=1):
        cmd ='I'+str(chan)+'O?\n'
        self.send_cmd(cmd)
        resp = self.recv_cmd()
        self.log.info("Current channel {}: {}A".format(chan,float(resp[:-3])))
        return float(resp[:-3])

    def unlock(self):
        cmd ='IFUNLOCK\n'
        self.send_cmd(cmd)

    def lock(self):
        cmd ='IFLOCK\n'
        self.send_cmd(cmd)

    def turn_on_chan(self, chan=1):
        cmd ='OP'+str(chan)+' 1\n'
        self.log.info("Turning ON Channel {}".format(chan))
        self.send_cmd(cmd)
        self.CHAN_STATUS[chan-1]=True

    def turn_off_chan(self, chan=1):
        cmd ='OP'+str(chan)+' 0\n'
        self.log.info("Turning OFF Channel {}".format(chan))
        self.send_cmd(cmd)
        self.CHAN_STATUS[chan-1]=False

    def turn_on(self):
        self.log.info("Turning On Both Channels")
        self.turn_on_chan(chan=1)
        self.turn_on_chan(chan=2)

    def turn_off(self):
        self.log.info("Turning Off Both Channels")
        self.turn_off_chan(chan=1)
        self.turn_off_chan(chan=2)

    def get_status_chan(self, chan=1):
        cmd ='OP'+str(chan)+'?\n'
        self.send_cmd(cmd)
        resp = self.recv_cmd()
        state = int(resp[0])-48
        if state == 0 or state == 1:
            self.log.info("The Channel {} is {}".format(chan,self.STATUS[state]))
            return bool(state)
        else:
            self.log.error("Something was wrong reading status of channel {}".format(chan))
            exit()

    def get_status(self):
        for j in range(2):
            self.CHAN_STATUS[j] = self.get_status_chan(chan=j+1)

    def setup_chan(self, chan=1, volts=10.0, amps=0.3):
        self.log.info("Setup Channel {}".format(chan))
        self.set_voltage(volts=volts, chan=chan)
        self.set_current(amps=amps, chan=chan)

    def setup(self, volts_1=10.0, amps_1=0.3, volts_2=10.0, amps_2=0.3):
        self.log.info("Setup Both Channels")
        self.setup_chan(chan=1, volts=volts_1, amps=amps_1)
        self.setup_chan(chan=2, volts=volts_2, amps=amps_2)

    def get_parameters_chan(self, chan=1):
        V = self.get_voltage(chan=chan)
        I = self.get_current(chan=chan)
        return V, I

    def get_parameters(self):
        V1, I1 = self.get_parameters_chan(chan=1)
        V2, I2 = self.get_parameters_chan(chan=2)
        return V1, V2, I1, I2

def main():
    parser = argparse.ArgumentParser(description='Control CPX400DP from USB', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--device',     "-D", type=str,   default='/dev/ttyACM0', help="Linux device name")
    parser.add_argument('--baudrate',   "-b", type=int,   default=115200,         help="Serial baudrate")
    parser.add_argument('--chan',       "-C", type=int,   default=1,              help="Channel selector")
    parser.add_argument('--volts',      "-V", type=float, default=12.0,           help="Volts")
    parser.add_argument('--amps',       "-A", type=float, default=1.0,            help="Amps")
    parser.add_argument("--powerup", action='store_true', help="Switch On requested channel with requested values")
    args = parser.parse_args()
    try:
        psu=CPX400DP(name='CPX400DP', devname=args.device, baudrate=args.baudrate, log_level=logging.INFO)
    except serial.SerialException:
        exit()
    psu.setup_chan(chan=args.chan, volts=args.volts, amps=args.amps)
    if args.powerup:
        psu.turn_on_chan(chan=args.chan)
    else:
        psu.turn_off_chan(chan=args.chan)
    psu.close()

if __name__ == '__main__':
    sys.exit(main())
