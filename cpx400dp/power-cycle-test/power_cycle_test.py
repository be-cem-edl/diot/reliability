'''

    name:   Alen Arias Vazquez
    mail:   alen.arias.vazquez@cern.ch
    group:  BE-CEM-EDL
    date:   22/03/2022
    Script to realize power cycle test in different devices with
    reliability purposes

'''

import sys
import os
import time
import multiprocessing
import logging, logging.config
import argparse

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from cpx400dp import CPX400DP

class PowerCycleTest(object):

    def __init__(self, dirname='dirname', logname='PowerCycleTest', devname='/dev/ttyACM0', baudrate=115200, log_level=logging.INFO):
        logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)s-%(levelname)-s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        self.logname = logname
        self.log = logging.getLogger(self.logname)
        self.dirname = dirname
        self.time_on = 0
        self.time_off = 0
        self.it = 0
        self.test_id = 0
        self.ext='.csv'
        self.test_finished=False
        self.psu=CPX400DP(devname=devname, baudrate=baudrate, log_level=log_level)

    def create_directory(self, dirname):
        if not os.path.exists(dirname):
            os.makedirs(dirname)

    def get_file_name(self, filename, number=0):
        found = False
        while (found==False):
            if not os.path.exists(self.dirname+'/'+filename+'_'+str(number)+self.ext):
                found = True
            else:
                number=number+1
        return self.dirname+'/'+filename+'_'+str(number)+self.ext

    def setup_test(self, time_on=2, time_off=4, it=2):
        self.create_directory(self.dirname)
        self.time_on = time_on
        self.time_off = time_off
        self.it = it
        self.log.info("Setup parameters to the test")

    def run_test_single(self, chan, V, I):
        self.log.info("Test in the channel {} starts".format(chan))
        self.psu.setup_chan(chan=chan, volts=V, amps=I)
        log_file_name=self.get_file_name(filename='test_chan'+str(chan),number=0)
        log_file=open(log_file_name, 'w')
        log_file.write("TIME,V,I,P\n")
        for i in range(self.it):
            diff_time = 0
            start_time=time.time()
            self.log.info("Iteration {} - Turn ON".format(i))
            self.psu.turn_on_chan(chan=chan)
            while (diff_time < self.time_on):
                V, I = self.psu.get_parameters_chan(chan=chan)
                log_file.write("{:.3f},{:.4f},{:.4f},{:.4f}\n".format(time.time(),V,I,V*I))
                diff_time = time.time() - start_time
                time.sleep(0.005)
            self.psu.turn_off_chan(chan=chan)
            diff_time = 0
            start_time=time.time()
            self.log.info("Iteration {} - Turn OFF".format(i))
            while (diff_time < self.time_off):
                V, I = self.psu.get_parameters_chan(chan=chan)
                log_file.write("{:.3f},{:.4f},{:.4f},{:.4f}\n".format(time.time(),V,I,V*I))
                diff_time = time.time() - start_time
                time.sleep(0.005)
        log_file.close()

    def run_test_dual(self, V1, V2, I1, I2):
        self.log.info("Test in both channels starts")
        self.psu.setup(volts_1=V1, amps_1=I1, volts_2=V2, amps_2=I2)
        log_file_name=self.get_file_name(filename='dual_test',number=0)
        log_file=open(log_file_name, 'w')
        log_file.write("TIME,V1,I1,P1,V2,I2,P2\n")
        for i in range(self.it):
            diff_time = 0
            start_time=time.time()
            self.log.info("Iteration {} - Turn ON".format(i))
            self.psu.turn_on()
            while (diff_time < self.time_on):
                V1, V2, I1, I2 = self.psu.get_parameters()
                log_file.write("{:.3f},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f},{:.4f}\n".format(time.time(),V1,I1,I1*V1,V2,I2,I2*V2))
                diff_time = time.time() - start_time
                time.sleep(0.005)
            self.psu.turn_off()
            diff_time = 0
            start_time=time.time()
            self.log.info("Iteration {} - Turn OFF".format(i))
            while (diff_time < self.time_off):
                log_file.write("{},{},{},{},{},{},{}\n".format(time.time(),0.0,0.0,0.0,0.0,0.0,0.0))
                diff_time = time.time() - start_time
                time.sleep(0.010)
        log_file.close()

def main():
    parser = argparse.ArgumentParser(description='Power Cycling Test', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--device',     "-D", type=str,   default='/dev/ttyACM0', help="Linux device name")
    parser.add_argument('--baudrate',   "-b", type=int,   default=115200,         help="Serial baudrate")
    parser.add_argument('--it',   type=int,   default=50,     help="Number of iterations")
    parser.add_argument('--on',    "-on",  type=int,   default=5,      help="Number of seconds that each channel is ON")
    parser.add_argument('--off',   "-off", type=int,   default=5,      help="Number of seconds that each channel is ON")
    parser.add_argument('--dir',   "-d",   type=str,   default='test', help="Directory to storage results")
    parser.add_argument('--chan',  "-C",   type=str,   default='1',    choices=['1','2','all'], help="Channel selector")
    parser.add_argument("-V1",  type=float, default=12.0,   help="Volts")
    parser.add_argument("-I1",  type=float, default=1.0,    help="Amps")
    parser.add_argument("-V2",  type=float, default=12.0,   help="Volts")
    parser.add_argument("-I2",  type=float, default=1.0,    help="Amps")
    args = parser.parse_args()

    test = PowerCycleTest(dirname=args.dir, devname=args.device, baudrate=args.baudrate)
    test.log.info("Results will be stored store at {}/".format(args.dir))
    test.log.info("The number of seconds ON:       {}".format(args.on))
    test.log.info("The number of seconds OFF:      {}".format(args.off))
    test.log.info("The number of power cycles:     {}".format(args.it))
    test.setup_test(time_on=args.on, time_off=args.off, it=args.it)
    if args.chan=='all':
        test.run_test_dual(V1=args.V1, V2=args.V2, I1=args.I1, I2=args.I2)
    else:
        if int(args.chan) == 1:
            test.run_test_single(chan=1,V=args.V1,I=args.I1)
        else:
            test.run_test_single(chan=2,V=args.V2,I=args.I2)
    test.psu.close()

if __name__ == '__main__':
    sys.exit(main())
