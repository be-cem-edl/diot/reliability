'''

    name:   Alen Arias Vazquez
    mail:   alen.arias.vazquez@cern.ch
    group:  BE-CEM-EDL
    date:   15/09/2023
    Linear regression script

'''
import sys
import os
import logging, logging.config
import matplotlib.pyplot as plt
import numpy as np

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

'''
    Data Colected from the readouts of the DIOT RT system board
'''
X_5V = np.array([
            [ 667, 664, 675 ], [ 693, 687, 694 ], [ 693, 691, 690 ], [ 763, 761, 762 ],
            [ 822, 821, 823 ], [ 871, 872, 872 ], [ 921, 924, 924 ], [ 975, 974, 973 ],
            [ 1024, 1027, 1026 ], [ 1078, 1078, 1076 ], [ 1128, 1130, 1129 ], [ 1180, 1181, 1182 ]
        ])

X_12V = np.array([
        [ 17 ], [ 50 ], [ 82 ], [ 115 ], [ 148 ], [ 164 ],[ 246 ], [ 328 ], [ 410 ], [ 492 ],
        [ 574 ], [ 656 ], [ 738 ], [ 820 ], [ 902 ], [ 984 ], [ 1065 ], [ 1147 ], [ 1229 ], [ 1311 ]
    ])


Y_5V = np.array([ 0.05, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0 ])
Y_12V = np.array([
                0.1, 0.3, 0.5, 0.7, 0.9, 1.0, 1.5, 2.0, 2.5, 3.0, 
                3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0
        ])
#Average correction value
CORR_I12V = 2.7

def do_calculate_average(data):
    data_avg = np.zeros(len(data))
    for i in range(len(data)):
        data_avg[i] = np.average(data[i])
    return data_avg

def do_calculate_average12(data, corr):
    data_avg = np.zeros(len(data))
    for i in range(len(data)):
        data_avg[i] = np.average(data[i])-corr
    return data_avg

def do_makeplot(title,x_axis, y_axis, y_axis_pred, xlabel, ylabel):
    plt.figure(figsize=(20.48,15.36))
    plt.rc('xtick',labelsize=14)
    plt.rc('ytick',labelsize=22)
    plt.title(title, fontsize = 22)
    plt.scatter(x_axis, y_axis.reshape((-1,1)), color="red")
    plt.plot(x_axis, y_axis_pred, color="blue", linewidth=3)
    plt.xlabel(xlabel, fontsize = 16)
    plt.ylabel(ylabel, fontsize = 22)
    plt.xticks(x_axis,rotation=45)
    plt.yticks()
    if not (os.path.isdir("img")):
        os.mkdir("img")
    plt.savefig("img/{}.png".format(title.replace(' ', '_')))

def main():

    logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)s-%(levelname)-s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
    log = logging.getLogger('Linear-Regression')

    # Create Average Y Vectors
    #log.info("Create average Y arrays")
    X_5V_avg = do_calculate_average(X_5V)
    X_12V_avg = do_calculate_average12(X_12V, CORR_I12V)

    # Reshape
    X_5V_r = X_5V_avg.reshape((-1,1))
    X_12V_r = X_12V_avg.reshape((-1,1))

    # Create linear regression object
    log.info("Define linear regression object")
    regr_5V = linear_model.LinearRegression()
    regr_12V = linear_model.LinearRegression()

    log.info("Training model")
    regr_5V.fit(X_5V_r, Y_5V)
    regr_12V.fit(X_12V_r, Y_12V)

    log.info("Calculate predicted model")
    Y_5V_pred = regr_5V.predict(X_5V_r)
    Y_12V_pred = regr_12V.predict(X_12V_r)

    log.info("5V Information")
    log.info("Y = X * Slope + Intercept")
    log.info("Intercept: {}".format(regr_5V.intercept_))
    log.info("Slope    : {}".format(regr_5V.coef_[0]))

    log.info("12V Information")
    log.info("Y = X * Slope + Intercept")
    log.info("Intercept: {}".format(regr_12V.intercept_))
    log.info("Slope    : {}".format(regr_12V.coef_[0]))

    log.info("Making plot 5V")
    do_makeplot("5V Current", x_axis=X_5V_avg, y_axis=Y_5V, y_axis_pred=Y_5V_pred, xlabel="ADC Counts", ylabel="Current [A]")

    log.info("Making plot 12V")
    do_makeplot("12V Current", x_axis=X_12V_avg, y_axis=Y_12V, y_axis_pred=Y_12V_pred, xlabel="ADC Counts", ylabel="Current [A]")

if __name__ == '__main__':
    sys.exit(main())
