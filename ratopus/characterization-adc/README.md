# Characterization ADC

The ADC model is AD7291BCPZ

RaToPUS v4.0:
- changed current measurement on 12V, 5V stays the same

## Datasheet

- [Datasheet](https://www.mouser.ch/datasheet/2/609/AD7291-3118786.pdf)

## Requirements to use the script
```console
sudo apt install python3-sklearn
pip install numpy matplotlib
```

## Execute script
```console
python3 linear_regression.py
```

## Results
```console
2024-10-07 10:26:18.446 Linear-Regression-INFO: Define linear regression object
2024-10-07 10:26:18.447 Linear-Regression-INFO: Training model
2024-10-07 10:26:18.449 Linear-Regression-INFO: Calculate predicted model
2024-10-07 10:26:18.449 Linear-Regression-INFO: 5V Information
2024-10-07 10:26:18.449 Linear-Regression-INFO: Y = X * Slope + Intercept
2024-10-07 10:26:18.449 Linear-Regression-INFO: Intercept: -2.4876465940595285
2024-10-07 10:26:18.449 Linear-Regression-INFO: Slope    : 0.0037904926623568776
2024-10-07 10:26:18.449 Linear-Regression-INFO: 12V Information
2024-10-07 10:26:18.449 Linear-Regression-INFO: Y = X * Slope + Intercept
2024-10-07 10:26:18.449 Linear-Regression-INFO: Intercept: 0.013618592380550965
2024-10-07 10:26:18.449 Linear-Regression-INFO: Slope    : 0.0061036089068967955
2024-10-07 10:26:18.449 Linear-Regression-INFO: Making plot 5V
2024-10-07 10:26:18.840 Linear-Regression-INFO: Making plot 12V
```

![](img/5V_Current.png)

![](img/12V_Current.png)

## Maintainers
- [Alén Arias Vázquez](mailto:alen.arias.vazquez@cern.ch)
- [Bernard Guncic](mailto:bernard.guncic@cern.ch)
- [Greg Daniluk](mailto:grzegorz.daniluk@cern.ch)


