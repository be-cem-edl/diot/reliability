# Distributed I/O Tier Reliability/Dependability

Main project: [DI/OT](https://ohwr.org/project/diot/wikis/Home)

## Project description

The [Distributed I/O Tier](https://ohwr.org/project/diot/wikis/Home) (DI/OT) hardware kit is currently being developed to serve within a variety of accelerator control systems including critical applications.
To ensure a high level of dependability in these applications a methodological approach is pursued defining a variety of activities during several life cycle phases.

## Maintainers
- [Greg Daniluk](mailto:grzegorz.daniluk@cern.ch)
- [Volker Schramm](mailto:volker.schramm@cern.ch)
- [Alén Arias Vázquez](mailti:alen.arias.vazquez@cern.ch)
