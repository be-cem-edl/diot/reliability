/******************************************************************************/
/*
 *
 * @file sys_util.c
 *
 * Library to get info about memory usage/cpu load
 *
 ******************************************************************************/

/***************************** Include Files **********************************/
#include "sys_util.h"

/******************************* Functions ************************************/
/******************************************************************************/
/**
 * Get information about memory usage
 * struct sysinfo {
 *     long uptime;                             //! Seconds since boot
 *     unsigned long loads[3];                  //! 1, 5, and 15 minute load averages
 *     unsigned long totalram;                  //! Total usable main memory size
 *     unsigned long freeram;                   //! Available memory size
 *     unsigned long sharedram;                 //! Amount of shared memory
 *     unsigned long bufferram;                 //! Memory used by buffers
 *     unsigned long totalswap;                 //! Total swap space size
 *     unsigned long freeswap;                  //! Swap space still available
 *     unsigned short procs;                    //! Number of current processes
 *     unsigned long totalhigh;                 //! Total high memory size
 *     unsigned long freehigh;                  //! Available high memory size
 *     unsigned int mem_unit;                   //! Memory unit size in bytes
 *     char _f[20-2*sizeof(long)-sizeof(int)];  //! Padding to 64 bytes
 * };
 *
 ******************************************************************************/
int get_memory_info (diot_login_s * tx_frame, int it) {

    int data_len = 0;
    int ret = 0;
    struct sysinfo *info;

    // Get memory data
    info = (struct sysinfo *) calloc (1,sizeof(struct sysinfo));
    ret = sysinfo(info);

    if ((! info) || (ret != 0)) {
        printf("Failed to get memory data\n");
        for (int i = 0;  i < C_MEM_DATA_SIZE; i ++) {
            tx_frame->buffer[it+i] = 0;
            data_len ++;
        }
    } else {

        for (int i = 0;  i < C_MEM_DATA_SIZE; i ++) {

            switch (i){
                case 0:
                    tx_frame->buffer[it+i] = (uint32_t) info->uptime;
                    break;
                case 1:
                case 2:
                case 3:
                    tx_frame->buffer[it+i] = (uint32_t) info->loads[i-1];
                    break;
                case 4:
                    tx_frame->buffer[it+i] = (uint32_t) info->totalram;
                    break;
                case 5:
                    tx_frame->buffer[it+i] = (uint32_t) info->freeram;
                    break;
                default:
                    break;
            }

#if DEBUG == 1
            printf("MemData %u |", tx_frame->buffer[it+i]);
#endif
            data_len ++;

        }

        free(info);

    }
    return data_len;

}