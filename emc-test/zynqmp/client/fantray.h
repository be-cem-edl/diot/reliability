/******************************************************************************/
/**
 *
 * @file fantray.h
 * Author: alen.arias.vazquez@cern.ch
 * Library to communicate with the computer for the login
 *
 ******************************************************************************/

#ifndef __FANTRAY_H
#define __FANTRAY_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files **********************************/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>

#include "diot_util_i2c.h"
#include "diot_util_error.h"
#include "diot_util_gpio.h"

/************************** Constant Definitions ******************************/
/**************************** Type Definitions ********************************/
/***************** Macros (Inline Functions) Definitions **********************/
#define FANTRAY_I2C     PSU_I2C
#define FANTRAY_I2C_ADDR    "12"
#define UC_RESET_MAGIC 0x5A

/************************** Variable Definitions ******************************/
/************************** Function Prototypes *******************************/
int fantray_hard_reset();
int fantray_soft_reset();

#ifdef __cplusplus
}
#endif

#endif /* __FANTRAY_H */

