/*
 * diot_util_i2c - i2c channels and addresses on DIOT system board
 *
 * Author: Adam Wujek for CERN
 * Copyright CERN 2021
 *
 */

#ifndef __DIOT_UTIL_I2C_H
#define __DIOT_UTIL_I2C_H

#if SB_VER == 1
#define MAX6639_I2C                         "9"
#define LM75_I2C                            "6"
#define IRPS5401_I2C                        "5"
#define PSU_I2C                             "3"
#define SI5341_I2C                          "7"
#define PB_EEPROM_I2C                       "4"
#elif SB_VER == 2
#define MAX6639_I2C                         "8"
#define LM75_I2C                            "5"
#define IRPS5401_I2C                        "4"
#define PSU_I2C                             "9"
#define SI5341_I2C                          "6"
#define PB_EEPROM_I2C                       "3"
#else
#error SB_VER not defined
#endif

#define FANTRAY_I2C                         PSU_I2C
#define FANTRAY_I2C_ADDR                    "12"

#define FANTRAY_I2C_REG_WRITTEN_FW_SIZE     0xD1
#define FANTRAY_I2C_REG_WRITTEN_FW_BLOCK    0xD2
#define FANTRAY_I2C_REG_WRITTEN_FW_CHKSUM   0xD3
#define FANTRAY_I2C_REG_LOCAL_FW_CHKSUM     0xD4
#define FANTRAY_I2C_REG_BOOT_NEW_FW         0xD5
#define FANTRAY_I2C_REG_RESET               0xD6
#define FANTRAY_I2C_REG_PEC                 0xD9

#define FANTRAY_FW_TYPE_BOOTLOADER          0x1
#define FANTRAY_FW_TYPE_MAIN                0x2

#define FMC_EEPROM_I2C                      "0"
#define FMC_EEPROM_I2C_ADDR                 "50"

#define PSU1_I2C_ADDR                       "18"
#define PSU2_I2C_ADDR                       "19"

#define IRPS5401_1_I2C_ADDR                 "44"
#define IRPS5401_2_I2C_ADDR                 "45"

#define LM75_1_I2C_ADDR                     "49"
#define LM75_2_I2C_ADDR                     "48"
#define LM75_3_I2C_ADDR                     "4a"

#define MAX6639_I2C_ADDR                    "2c"

#define SI5341_I2C_ADDR                     "76"

#define PB_EEPROM_I2C_ADDR                  "50"

#define FANTRAY_DRIVER_PATH                 "/sys/bus/i2c/drivers/monimod"
#define FANTRAY_FULL_I2C_ADDR               FANTRAY_I2C "-00" FANTRAY_I2C_ADDR
#define FANTRAY_HWMON_SYS_PATH              "/sys/devices/platform/amba/ff030000.i2c/i2c-1/i2c-" FANTRAY_I2C "/" FANTRAY_I2C "-00" FANTRAY_I2C_ADDR "/hwmon"
#define PSU1_HWMON_SYS_PATH                 "/sys/devices/platform/amba/ff030000.i2c/i2c-1/i2c-" PSU_I2C "/" PSU_I2C "-00" PSU1_I2C_ADDR "/hwmon"
#define PSU2_HWMON_SYS_PATH                 "/sys/devices/platform/amba/ff030000.i2c/i2c-1/i2c-" PSU_I2C "/" PSU_I2C "-00" PSU2_I2C_ADDR "/hwmon"
#define DEBUGFS_PMBUS                       "/sys/kernel/debug/pmbus"

#endif /* __DIOT_UTIL_I2C_H */
