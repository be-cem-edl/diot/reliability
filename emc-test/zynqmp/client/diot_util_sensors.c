/******************************************************************************/
/*
 * diot_util - diagnostic tool for DIOT system board
 *
 * Author: Adam Wujek for CERN
 * Copyright CERN 2021
 *
 * Author: alen.arias.vazquez@cern.ch
 * Modified to use as library for login
 *
 ******************************************************************************/

/***************************** Include Files **********************************/
#include "diot_util_sensors.h"

/************************** Variable Definitions ******************************/
/* List of Temperature sensors */
static struct hw_sensor hw_sensors_temp_list[] = {
    DECLARE_DIOT_SENSOR("FPGA",  "max6639-i2c-" MAX6639_I2C "-" MAX6639_I2C_ADDR,      "Temp FPGA"),
    DECLARE_DIOT_SENSOR("FMC",   "lm75a-i2c-" LM75_I2C "-" LM75_1_I2C_ADDR,            "Temp FMC"),
    DECLARE_DIOT_SENSOR("DDR",   "lm75a-i2c-" LM75_I2C "-" LM75_2_I2C_ADDR,            "Temp DDR"),
    DECLARE_DIOT_SENSOR("Pow M", "lm75a-i2c-" LM75_I2C "-" LM75_3_I2C_ADDR,            "Temp Power Management"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P0V85 Temp"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8_AUX  Temp"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2 Temp"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8 Temp"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2_PSPLL LDO Temp"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 P3V3 Temp"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V8 Temp"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_0V9 Temp"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V2 Temp"),
    DECLARE_DIOT_SENSOR("PSU1",  "D57500520-i2c-" PSU_I2C "-" PSU1_I2C_ADDR,           "PSU1 Temp1"),
    DECLARE_DIOT_SENSOR("PSU1",  "D57500520-i2c-" PSU_I2C "-" PSU1_I2C_ADDR,           "PSU1 Temp2"),
    DECLARE_DIOT_SENSOR("PSU2",  "D57500520-i2c-" PSU_I2C "-" PSU2_I2C_ADDR,           "PSU2 Temp1"),
    DECLARE_DIOT_SENSOR("PSU2",  "D57500520-i2c-" PSU_I2C "-" PSU2_I2C_ADDR,           "PSU2 Temp2"),
    DECLARE_DIOT_SENSOR(NULL, NULL, NULL)
};

/* List of voltage, current, power (vcp) sensors */
static struct hw_sensor hw_sensors_vcp_list[] = {
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 Input Current"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 Input Power"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 Input Voltage"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P0V85 Current"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P0V85 Power"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P0V85 Voltage"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2 Current"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2 Power"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2 Voltage"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2_PSPLL LDO Current"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2_PSPLL LDO Power"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V2_PSPLL LDO Voltage"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8_AUX Current"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8_AUX Power"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8_AUX Voltage"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8 Current"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8 Power"),
    DECLARE_DIOT_SENSOR("IRPS1", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_1_I2C_ADDR, "IRPS1 P1V8 Voltage"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 Input Current"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 Input Power"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 Input Voltage"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_0V9 Current"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_0V9 Power"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_0V9 Voltage"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V2 Current"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V2 Power"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V2 Voltage"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V8 Current"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V8 Power"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 MGT_1V8 Voltage"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 P3V3 Current"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 P3V3 Power"),
    DECLARE_DIOT_SENSOR("IRPS2", "irps5401-i2c-" IRPS5401_I2C "-" IRPS5401_2_I2C_ADDR, "IRPS2 P3V3 Voltage"),
    DECLARE_DIOT_SENSOR("PSU1",  "D57500520-i2c-" PSU_I2C "-" PSU1_I2C_ADDR,           "PSU1 12V Voltage"),
    DECLARE_DIOT_SENSOR("PSU1",  "D57500520-i2c-" PSU_I2C "-" PSU1_I2C_ADDR,           "PSU1 12V Current"),
    DECLARE_DIOT_SENSOR("PSU1",  "D57500520-i2c-" PSU_I2C "-" PSU1_I2C_ADDR,           "PSU1 12V Power"),
    DECLARE_DIOT_SENSOR("PSU2",  "D57500520-i2c-" PSU_I2C "-" PSU2_I2C_ADDR,           "PSU2 12V Voltage"),
    DECLARE_DIOT_SENSOR("PSU2",  "D57500520-i2c-" PSU_I2C "-" PSU2_I2C_ADDR,           "PSU2 12V Current"),
    DECLARE_DIOT_SENSOR("PSU2",  "D57500520-i2c-" PSU_I2C "-" PSU2_I2C_ADDR,           "PSU2 12V Power"),
    DECLARE_DIOT_SENSOR(NULL, NULL, NULL)
};

static struct hw_sensor hw_sensors_monimod_list[] = {
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FANTRAY 12V Voltage"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FANTRAY 5V Voltage"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FAN1 Current"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FAN2 Current"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FAN3 Current"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FAN1 RPM Slots S+P1-4"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FAN2 RPM Slots P5-8"),
    DECLARE_DIOT_SENSOR("FANTRAY",   "monimod-i2c-" PSU_I2C "-" FANTRAY_I2C_ADDR,      "FAN3 RPM Power Supp"),
    DECLARE_DIOT_SENSOR(NULL, NULL, NULL)
};

static struct sensor_short temp_all_list[] = {
    DECLARE_SENSOR_SHORT("FPGA", "Temp FPGA"),
    DECLARE_SENSOR_SHORT("FMC", "Temp FMC"),
    DECLARE_SENSOR_SHORT("DDR", "Temp DDR"),
    DECLARE_SENSOR_SHORT("Pow M", "Temp Power Management"),
    // IRPS1 TEMP
    DECLARE_SENSOR_SHORT("IRPS1", "IRPS1 P0V85 Temp"),
    DECLARE_SENSOR_SHORT("IRPS1", "IRPS1 P1V8_AUX  Temp"),
    DECLARE_SENSOR_SHORT("IRPS1", "IRPS1 P1V2 Temp"),
    DECLARE_SENSOR_SHORT("IRPS1", "IRPS1 P1V8 Temp"),
    DECLARE_SENSOR_SHORT("IRPS1", "IRPS1 P1V2_PSPLL LDO Temp"),
    // IRPS2 TEMP
    DECLARE_SENSOR_SHORT("IRPS2", "IRPS2 P3V3 Temp"),
    DECLARE_SENSOR_SHORT("IRPS2", "IRPS2 MGT_1V8 Temp"),
    DECLARE_SENSOR_SHORT("IRPS2", "IRPS2 MGT_0V9 Temp"),
    DECLARE_SENSOR_SHORT("IRPS2", "IRPS2 MGT_1V2 Temp"),
    // PSU1 TEMP
    DECLARE_SENSOR_SHORT("PSU1", "PSU1 Temp1"),
    DECLARE_SENSOR_SHORT("PSU1", "PSU1 Temp2"),
    DECLARE_SENSOR_SHORT("PSU2", "PSU2 Temp1"),
    DECLARE_SENSOR_SHORT("PSU2", "PSU2 Temp2"),
    DECLARE_SENSOR_SHORT(NULL, NULL),
};

static struct short_vcp vcp_all_entries[] = {
    //! IRPS 1
    DECLARE_SHORT_VCP("Input",          "IRPS1 Input Voltage",          "IRPS1 Input Current",          "IRPS1 Input Power"),
    DECLARE_SHORT_VCP("P0V85",          "IRPS1 P0V85 Voltage",          "IRPS1 P0V85 Current",          "IRPS1 P0V85 Power"),
    DECLARE_SHORT_VCP("P1V8_AUX",       "IRPS1 P1V8_AUX Voltage",       "IRPS1 P1V8_AUX Current",       "IRPS1 P1V8_AUX Power"),
    DECLARE_SHORT_VCP("P1V2",           "IRPS1 P1V2 Voltage",           "IRPS1 P1V2 Current",           "IRPS1 P1V2 Power"),
    DECLARE_SHORT_VCP("P1V8",           "IRPS1 P1V8 Voltage",           "IRPS1 P1V8 Current",           "IRPS1 P1V8 Power"),
    DECLARE_SHORT_VCP("P1V2_PSPLL LDO", "IRPS1 P1V2_PSPLL LDO Voltage", "IRPS1 P1V2_PSPLL LDO Current", "IRPS1 P1V2_PSPLL LDO Power"),
    //! IRPS 2
    DECLARE_SHORT_VCP("Input",          "IRPS2 Input Voltage",          "IRPS2 Input Current",          "IRPS2 Input Power"),
    DECLARE_SHORT_VCP("P3V3",           "IRPS2 P3V3 Voltage",           "IRPS2 P3V3 Current",           "IRPS2 P3V3 Power"),
    DECLARE_SHORT_VCP("MGT_1V8",        "IRPS2 MGT_1V8 Voltage",        "IRPS2 MGT_1V8 Current",        "IRPS2 MGT_1V8 Power"),
    DECLARE_SHORT_VCP("MGT_0V9",        "IRPS2 MGT_0V9 Voltage",        "IRPS2 MGT_0V9 Current",        "IRPS2 MGT_0V9 Power"),
    DECLARE_SHORT_VCP("MGT_1V2",        "IRPS2 MGT_1V2 Voltage",        "IRPS2 MGT_1V2 Current",        "IRPS2 MGT_1V2 Power"),
    //! PSU 1
    DECLARE_SHORT_VCP("PSU1",           "PSU1 12V Voltage",             "PSU1 12V Current",             "PSU1 12V Power"),
    //! PSU 2
    DECLARE_SHORT_VCP("PSU2",           "PSU2 12V Voltage",             "PSU2 12V Current",             "PSU2 12V Power"),
    DECLARE_SHORT_VCP(NULL, NULL, NULL, NULL),
};

static struct sensor_short sensors_monimod_list[] = {
    DECLARE_SENSOR_SHORT("FANTRAY", "FANTRAY 12V Voltage"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FANTRAY 5V Voltage"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FAN1 Current"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FAN2 Current"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FAN3 Current"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FAN1 RPM Slots S+P1-4"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FAN2 RPM Slots P5-8"),
    DECLARE_SENSOR_SHORT("FANTRAY", "FAN3 RPM Power Supp"),
    DECLARE_SENSOR_SHORT(NULL, NULL),
};

/******************************* Functions ************************************/
static int get_input_value(const sensors_chip_name *name, const sensors_subfeature *sub, double *val) {

    int err;
    err = sensors_get_value(name, sub->number, val);
    if (err && err != -SENSORS_ERR_ACCESS_R) {
        diot_error_add("ERROR: Can't get value of subfeature %s: %s\n", sub->name, sensors_strerror(err));
    }
    return err;

}

static struct hw_sensor * find_hw_sensor(char *sensor_name, struct hw_sensor *hw_sensors_list) {

    struct hw_sensor *sensor_p = hw_sensors_list;
    for (; sensor_p->chip_name; sensor_p++) {
        if (!strcmp(sensor_name, sensor_p->sensor_name)) {
            /* sensor's name should be uniq */
            return sensor_p;
        }
    }
    return NULL;

}

static void get_sensor_values(struct hw_sensor *sensor_to_read) {

    sensors_chip_name chip;
    const sensors_chip_name *chip_selected;
    const sensors_feature *feature;
    const sensors_subfeature *sf;
    int i_feature;
    char *label = NULL;
    int sf_i;

    /* clear sensor's values */
    sensor_to_read->read_val = NAN;
    sensor_to_read->min = NAN;
    sensor_to_read->max = NAN;
    sensor_to_read->crit_min = NAN;
    sensor_to_read->crit_max = NAN;
    sensor_to_read->emerg = NAN;
    sensor_to_read->alarm = 0;

    if (!sensor_to_read->chip_name) {
        diot_error_add("ERROR: Chip name empty\n");
        return;
    }

    if (!sensor_to_read->sensor_name) {
        diot_error_add("ERROR: Sensor name empty, chip name %s\n", sensor_to_read->chip_name);
        return;
    }

    if (sensors_parse_chip_name(sensor_to_read->chip_name, &chip)) {
        diot_error_add("ERROR: Chip %s not available (parse error?)\n", sensor_to_read->chip_name);
        return;
    }

    int tmp = 0;
    chip_selected = sensors_get_detected_chips(&chip, &tmp);
    if (!chip_selected) {
        diot_error_add("ERROR: Chip %s not available\n", sensor_to_read->chip_name);
        sensors_free_chip_name(&chip);
        return;
    }

    i_feature = 0;
    while ((feature = sensors_get_features(chip_selected, &i_feature))) {

        if (!(label = sensors_get_label(chip_selected, feature))) {
            printf("%6s |", "l ERR");
            diot_error_add("ERROR: Can't get label of sensor %s!\n", feature->name);
            continue;
        }

        if (strcmp(label, sensor_to_read->sensor_name)) {
            /* not this sensor */
            free(label);
            label = NULL;
            continue;
        }

        /* check type? */
        sf_i = 0;
        while ((sf = sensors_get_all_subfeatures(chip_selected, feature, &sf_i))) {
            switch (sf->type) {
                case SENSORS_SUBFEATURE_IN_INPUT:
                case SENSORS_SUBFEATURE_FAN_INPUT:
                case SENSORS_SUBFEATURE_TEMP_INPUT:
                case SENSORS_SUBFEATURE_POWER_INPUT:
                case SENSORS_SUBFEATURE_ENERGY_INPUT:
                case SENSORS_SUBFEATURE_CURR_INPUT:
                case SENSORS_SUBFEATURE_HUMIDITY_INPUT:
                    get_input_value(chip_selected, sf, &sensor_to_read->read_val);
                    break;

                case SENSORS_SUBFEATURE_IN_MIN:
                case SENSORS_SUBFEATURE_FAN_MIN:
                case SENSORS_SUBFEATURE_TEMP_MIN:
                case SENSORS_SUBFEATURE_CURR_MIN:
                    get_input_value(chip_selected, sf, &sensor_to_read->min);
                    break;

                case SENSORS_SUBFEATURE_IN_MAX:
                case SENSORS_SUBFEATURE_FAN_MAX:
                case SENSORS_SUBFEATURE_TEMP_MAX:
                case SENSORS_SUBFEATURE_POWER_MAX:
                case SENSORS_SUBFEATURE_CURR_MAX:
                    get_input_value(chip_selected, sf, &sensor_to_read->max);
                    break;

                case SENSORS_SUBFEATURE_IN_LCRIT:
                case SENSORS_SUBFEATURE_TEMP_LCRIT:
                case SENSORS_SUBFEATURE_CURR_LCRIT:
                    get_input_value(chip_selected, sf, &sensor_to_read->crit_min);
                    break;

                case SENSORS_SUBFEATURE_IN_CRIT:
                case SENSORS_SUBFEATURE_TEMP_CRIT:
                case SENSORS_SUBFEATURE_POWER_CRIT:
                case SENSORS_SUBFEATURE_CURR_CRIT:
                    get_input_value(chip_selected, sf, &sensor_to_read->crit_max);
                    break;

                case SENSORS_SUBFEATURE_TEMP_EMERGENCY:
                    get_input_value(chip_selected, sf, &sensor_to_read->emerg);
                    break;

                /* check if subfeature is an alarm */
                default:
                    break;

            }
        }

        free(label);
        label = NULL;
        break;
    }

    sensors_free_chip_name(&chip);

}

static void read_one_sensor(char *sensor_to_read, struct hw_sensor *hw_sensors_list) {

    struct hw_sensor *sensor_p;
    sensor_p = find_hw_sensor(sensor_to_read, hw_sensors_list);
    if (sensor_p) {
        get_sensor_values(sensor_p);
    }

}



static void read_sensors(struct sensor_short *sensors_to_read_list, struct hw_sensor *hw_sensors_list) {

    struct sensor_short *sensor_to_read = sensors_to_read_list;
    for (; sensor_to_read->short_name; sensor_to_read++) {
       read_one_sensor(sensor_to_read->sensor_name, hw_sensors_list);
    }

}

static void read_sensors_vcp(struct short_vcp *sensors_to_read_list, struct hw_sensor *hw_sensors_list) {

    struct short_vcp *sensor_to_read = sensors_to_read_list;
    for (; sensor_to_read->short_name; sensor_to_read++) {
        read_one_sensor(sensor_to_read->voltage_sensor_name, hw_sensors_list);
        read_one_sensor(sensor_to_read->current_sensor_name, hw_sensors_list);
        read_one_sensor(sensor_to_read->power_sensor_name, hw_sensors_list);
    }

}

void read_all_sensors(void) {

    read_sensors(temp_all_list, hw_sensors_temp_list);
    read_sensors_vcp(vcp_all_entries, hw_sensors_vcp_list);
    read_sensors(sensors_monimod_list, hw_sensors_monimod_list);
    diot_error_clear();

}

int get_sensor_data(diot_login_s * tx_frame, int it) {

    int i;
    int data_len = 0;
    struct hw_sensor *hw_sens;
    struct short_vcp *vcp_reader = vcp_all_entries;
    struct hw_sensor *sensor_v;
    struct hw_sensor *sensor_i;
    struct hw_sensor *sensor_mon;

    int it_aux = it;

     /* Read temperature sensors */
    for (i = 0; temp_all_list[i].sensor_name; i++) {
        hw_sens = find_hw_sensor(temp_all_list[i].sensor_name, hw_sensors_temp_list);
        if (!hw_sens) {
            diot_error_add("BUG: sensor %s not on the hw sensors list!\n", temp_all_list[i].sensor_name);
            continue;
        }

#if DEBUG == 1
        printf("%s --- ", hw_sens->sensor_name);
#endif
        if (!isnan(hw_sens->read_val)) {
#if DEBUG == 1
            printf("%.6f --- %u |", hw_sens->read_val, (uint32_t)(hw_sens->read_val * C_MUL_FLOAT));
#endif
            tx_frame->buffer[it_aux] = (uint32_t)(hw_sens->read_val * C_MUL_FLOAT);
        } else {
#if DEBUG == 1
            printf("%s |","N/A");
#endif
            tx_frame->buffer[it_aux] = 0xDEADBEEF;
        }

        data_len = data_len +1;
        it_aux = it_aux + 1;

    }

    /* Read Voltage & Current */
    for (; vcp_reader->short_name; vcp_reader++) {
#if DEBUG == 1
        printf("%s --- ",vcp_reader->short_name);
#endif
        sensor_v = find_hw_sensor(vcp_reader->voltage_sensor_name, hw_sensors_vcp_list);
        sensor_i = find_hw_sensor(vcp_reader->current_sensor_name, hw_sensors_vcp_list);

        if (sensor_v) {
#if DEBUG == 1
            printf("%lf --- %u |", sensor_v->read_val, (uint32_t)(sensor_v->read_val * C_MUL_FLOAT));
#endif
            tx_frame->buffer[it_aux]=(uint32_t)(sensor_v->read_val * C_MUL_FLOAT);
        } else {
            tx_frame->buffer[it_aux] = 0xDEADBEEF;
        }

        data_len = data_len +1;
        it_aux = it_aux + 1;

#if DEBUG == 1
        printf("%s --- ",vcp_reader->short_name);
#endif

        if (sensor_i) {
#if DEBUG == 1
            printf("%lf ---  %u |", sensor_i->read_val, (uint32_t)(sensor_i->read_val * C_MUL_FLOAT));
#endif
            tx_frame->buffer[it_aux]=(uint32_t)(sensor_i->read_val * C_MUL_FLOAT);
        } else {
#if DEBUG == 1
            printf("%s |","N/A");
#endif
            tx_frame->buffer[it_aux] = 0xDEADBEEF;
        }

        data_len = data_len +1;
        it_aux = it_aux + 1;
    }

     /* Read Monimod sensors */
    for (i = 0; sensors_monimod_list[i].sensor_name; i++) {
        sensor_mon = find_hw_sensor(sensors_monimod_list[i].sensor_name, hw_sensors_monimod_list);
        if (!sensor_mon) {
            diot_error_add("BUG: sensor %s not on the hw sensors list!\n", sensors_monimod_list[i].sensor_name);
            continue;
        }

#if DEBUG == 1
        printf("%s --- ", sensor_mon->sensor_name);
#endif
        if (!isnan(sensor_mon->read_val)) {
#if DEBUG == 1
            printf("%.6f --- %u |", sensor_mon->read_val, (uint32_t)(sensor_mon->read_val * C_MUL_FLOAT));
#endif
            tx_frame->buffer[it_aux] = (uint32_t)(sensor_mon->read_val * C_MUL_FLOAT);
        } else {
#if DEBUG == 1
            printf("%s |","N/A");
#endif
            tx_frame->buffer[it_aux] = 0xDEADBEEF;
        }

        data_len = data_len +1;
        it_aux = it_aux + 1;

    }

    return data_len;

}
