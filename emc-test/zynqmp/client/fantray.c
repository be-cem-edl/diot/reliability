/******************************************************************************/
/*
 *
 * @file fantray.c
 *
 * @brief Login Frantray RPMs
 *
 ******************************************************************************/

/***************************** Include Files **********************************/
#include "fantray.h"

/************************** Variable Definitions ******************************/
/******************************* Functions ************************************/
/******************************************************************************/
/**
 * @brief Bind/Unbind I2C driver
 *
 ******************************************************************************/
static int fantray_bind_driver(char *bind_path) {

    FILE *bind_file;
    char *msg;
    int ret = 0;

    /* Unbind monimod */
    /* Equivalent of echo "9-0012" > /sys/bus/i2c/drivers/monimod/(un)bind */
    bind_file = fopen(bind_path, "w");

    if (!bind_file) {
        diot_error_add("Unable to open bind file (%s) for monimod\n",
        bind_path);
        return -1;
    }

    msg = FANTRAY_FULL_I2C_ADDR;
    (void)! fwrite(msg, sizeof(msg), 1, bind_file);

    if (ferror(bind_file)) {
        diot_error_add("Error while writing file (%s)\n", bind_path);
        ret = -1;
    }

    fclose(bind_file);

    return ret;

}

/******************************************************************************/
/**
 * @brief Open I2C device
 *
 ******************************************************************************/
static int open_i2c_dev(int i2cbus) {

    int i2c_file;
    char filename[20];

    sprintf(filename, "/dev/i2c-%d", i2cbus);
    i2c_file = open(filename, O_RDWR);

    if (i2c_file < 0) {
        diot_error_add("Error: Could not open file \"/dev/i2c-%d\": %s\n", i2cbus, i2cbus, strerror(errno));
    }

    return i2c_file;
}

/******************************************************************************/
/**
 * @brief Set slave address
 *
 ******************************************************************************/
static int set_slave_addr(int i2c_file, int address, int force) {

    int ret = 0;

    /* With force, let the user read from/write to the registers even when a driver is also running */
    if ((ret = ioctl(i2c_file, force ? I2C_SLAVE_FORCE : I2C_SLAVE, address) < 0)) {
        diot_error_add("Error: Could not set address to 0x%02x: %s\n", address, strerror(errno));
        return ret;
    }

    return ret;
}

/******************************************************************************/
/**
 * @brief Fantray Reset Hard
 *
 ******************************************************************************/
int fantray_hard_reset(){

    int rc;
    int ret;

    diot_error_clear();
    /* Unbind the Fantray from the monimod driver */
    /* Ignore errors of unbind */
    rc = fantray_bind_driver(FANTRAY_DRIVER_PATH "/unbind");

    /* Reset Fantray */
    ret = gpio_fantray_reset(0);
    if (ret < 0) {
        printf("Unable to reset Fantray (ret = %d)\n", ret);
    }

    sleep(1);

    ret = gpio_fantray_reset(1);
    if (ret < 0) {
        printf("Unable to reset Fantray (ret = %d)\n", ret);
    }

    /* Give the Fantray some time to start */
    sleep(2);

    /* Bind the Fantray to the monimod driver */
    rc = fantray_bind_driver(FANTRAY_DRIVER_PATH "/bind");

    diot_error_print();
    if (ret) {
        return -1;
    }

    return rc;

}

/******************************************************************************/
/**
 * @brief Fantray Reset Soft
 *
 ******************************************************************************/
int fantray_soft_reset() {

    int i2cbus, i2c_file;
    int ret = 0;
    int use_pec;

    i2cbus = atoi(FANTRAY_I2C);
    i2c_file = open_i2c_dev(i2cbus);
    if (i2c_file < 0) {
        printf("Error: Failed to open device: %s\n", strerror(errno));
        return i2c_file;
    }

    if ((ret = set_slave_addr(i2c_file, strtol(FANTRAY_I2C_ADDR, NULL, 16), 0)) < 0) {
        close(i2c_file);
        return ret;
    }

    /* If PEC is used by Monimod, it has to be used here too, otherwise writes are silently rejected */
    /* Read PEC */
    use_pec = i2c_smbus_read_byte_data(i2c_file, FANTRAY_I2C_REG_PEC);
    if (use_pec > 0) {
        /* PEC enabled, use it */
        if (ioctl(i2c_file, I2C_PEC, 1) < 0) {
            printf("Error: Could not set PEC: %s\n", strerror(errno));
        } else {
            printf("Using PEC\n");
        }
    }

    ret = i2c_smbus_write_byte_data(i2c_file, FANTRAY_I2C_REG_RESET, UC_RESET_MAGIC);

    close(i2c_file);
    return ret;

}
