/******************************************************************************/
/**
 *
 * @file sys_util.h
 * Author: alen.arias.vazquez@cern.ch
 * Library to get info about memory usage/cpu load
 *
 ******************************************************************************/

#ifndef __SYS_UTIL_H
#define __SYS_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files **********************************/
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/sysinfo.h>

#include "communication.h"

/************************** Constant Definitions ******************************/
/***************** Macros (Inline Functions) Definitions **********************/
#ifndef C_MEM_DATA_SIZE
#define C_MEM_DATA_SIZE 6
#endif

/**************************** Type Definitions ********************************/
/************************** Variable Definitions ******************************/
/************************** Function Prototypes *******************************/
int get_memory_info (diot_login_s * tx_frame, int it);

#ifdef __cplusplus
}
#endif

#endif /* __SYS_UTIL_H */
