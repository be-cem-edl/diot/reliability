/*
 * diot_util - diagnostic tool for DIOT system board
 *
 * Author: Adam Wujek for CERN
 * Copyright CERN 2021
 *
 */

#ifndef __DIOT_UTIL_GPIO_H
#define __DIOT_UTIL_GPIO_H

int cmd_print_status(char *params);
int cmd_led2(char *params);
int cmd_peripheral_rst(char *params);
int cmd_power_cycle(char *params);

int open_gpios(void);
int close_gpios(void);

int read_pcb_ver(void);
int read_mezzanine_presence(void);
int read_psu_presence(int psu);
int read_slot_use(int slot);
int read_system_slot_presence(void);
int read_psu_alert(void);
int read_pb_eeprom(uint8_t *eeprom_buff, size_t eeprom_size, int slot);
int read_fmc_eeprom(uint8_t *eeprom_buff, size_t eeprom_size);
int psu_added_or_removed(int psu);
int restore_i2c(void);
int gpio_fantray_reset(int val);

#endif /* __DIOT_UTIL_GPIO_H */
