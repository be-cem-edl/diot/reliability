/*
 * diot_util - diagnostic tool for DIOT system board
 *
 * Author: Adam Wujek for CERN
 * Copyright CERN 2021
 *
 * Author: alen.arias.vazquez@cern.ch
 * Modified to use as library
 *
 */

#ifndef __DIOT_UTIL_SENSORS_H
#define __DIOT_UTIL_SENSORS_H

#ifdef __cplusplus
extern "C" {
#endif
/***************************** Include Files **********************************/
#include <stdio.h>
#include <string.h>
#include <gpiod.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>

#include <readline/readline.h>
#include <readline/history.h>
#include <sensors/sensors.h>
#include <sensors/error.h>

#include "diot_util_error.h"
#include "diot_util_gpio.h"
#include "diot_util_i2c.h"
#include "communication.h"

/************************** Constant Definitions ******************************/
/***************** Macros (Inline Functions) Definitions **********************/
#ifndef C_MUL_FLOAT
#define C_MUL_FLOAT 1000
#endif

#ifndef COLOR_RED
#define COLOR_RED "\033[1;31]m"
#endif

#ifndef COLOR_OFF
#define COLOR_OFF "\033[0m"
#endif


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define ARRAY_SIZE(arr)     (sizeof(arr) / sizeof((arr)[0]))
#define ALARM_CHECK(x) x ? COLOR_RED: ""
#define ALARM_OFF(x) x ? COLOR_OFF: ""

#define DECLARE_DIOT_SENSOR(_short_name, _chip_name, _sensor_name) \
    { \
        .short_name = _short_name, \
        .chip_name = _chip_name, \
        .sensor_name = _sensor_name \
    }

#define DECLARE_ALARM(_alarm_type, _alarm_msg) \
    { \
        .alarm_type = _alarm_type, \
        .alarm_msg = _alarm_msg \
    }

#define DECLARE_SENSOR_SHORT(_short_name, _sensor_name) \
    { \
        .short_name = _short_name, \
        .sensor_name = _sensor_name \
    }

#define DECLARE_SHORT_VCP(_short_name, _voltage_sensor_name, _current_sensor_name, _power_sensor_name) \
    { \
        .short_name = _short_name, \
        .voltage_sensor_name = _voltage_sensor_name, \
        .current_sensor_name = _current_sensor_name, \
        .power_sensor_name = _power_sensor_name, \
    }

/**************************** Type Definitions ********************************/
struct hw_sensor {
    char *short_name;
    char *chip_name;
    char *sensor_name; /* if null take max value from all sensors in the chip */
    sensors_feature_type sensor_type;
    double read_val;
    double min;
    double max;
    double crit_min;
    double crit_max;
    double emerg;
    int alarm;
};

struct sensor_short {
    char *short_name;
    char *sensor_name; /* if null take max value from all sensors in the chip */
};

struct short_vcp { /* vcp = Voltage, Current, Power */
    char *short_name;
    char *voltage_sensor_name;
    char *current_sensor_name;
    char *power_sensor_name;
};

struct sensor_alarms {
    sensors_subfeature_type alarm_type;
    char *alarm_msg;
};

/************************** Variable Definitions ******************************/
/************************** Function Prototypes *******************************/
void read_all_sensors(void);
void print_all(void);

//int get_sensor_data(char * payload);
int get_sensor_data(diot_login_s * tx_frame, int it);

#ifdef __cplusplus
}
#endif
#endif /* __DIOT_UTIL_SENSORS_H */
