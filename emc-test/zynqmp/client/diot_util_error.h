/*
 * diot_util - diagnostic tool for DIOT system board
 *
 * Author: Adam Wujek for CERN
 * Copyright CERN 2021
 *
 */

#ifndef __DIOT_UTIL_ERROR_H
#define __DIOT_UTIL_ERROR_H

void diot_error_clear(void);
void diot_error_print(void);
void diot_error_add(const char* format, ...);
void diot_error_add_to_secondary_buff(void);
void diot_error_move_from_secondary_buff(void);
int diot_error_buff_n_errors(void);

#endif /* __DIOT_UTIL_ERROR_H */
