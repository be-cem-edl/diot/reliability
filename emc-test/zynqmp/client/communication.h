/******************************************************************************/
/**
 *
 * @file communication.h
 * Author: alen.arias.vazquez@cern.ch
 * Library to communicate with the computer for the login
 *
 ******************************************************************************/

#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files **********************************/
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

/************************** Constant Definitions ******************************/
/***************** Macros (Inline Functions) Definitions **********************/
#ifndef C_HEADER
#define C_HEADER 0x454D4354
#endif

#ifndef C_MAX_BUFFER_SIZE_B
#define C_MAX_BUFFER_SIZE_B 1024
#define C_MAX_BUFFER_SIZE_W (C_MAX_BUFFER_SIZE_B/4)
#endif

// 6 fields used for HEADER, PKG Number, Timestamp x2, Length util, CRC
#define C_MAX_DATA_SIZE C_MAX_BUFFER_SIZE-6

#ifndef C_MAX_RTT
#define C_MAX_RTT 32
#endif

#ifndef C_LOG_PERIOD
#define C_LOG_PERIOD 0.0
#endif

#define C_CRC_INIT 0x0

/**************************** Type Definitions ********************************/
typedef struct sensor_frame_t {
    uint32_t header;
    uint32_t pkg_number;
    uint32_t len;
    uint32_t time_sec;
    uint32_t time_nsec;
    char payload[C_MAX_BUFFER_SIZE_B];
    uint32_t crc;
} sensor_frame_s;

typedef struct diot_login_t {
    uint32_t buffer[C_MAX_BUFFER_SIZE_W];
} diot_login_s;

/************************** Variable Definitions ******************************/
/************************** Function Prototypes *******************************/
int open_socket(void);
void connect_to_server(int sockfd, struct sockaddr_in server_addr);
void fill_header(diot_login_s * tx_frame, uint32_t pkg_number);
uint32_t singletable_crc32c(uint32_t crc, const void *buf, size_t size);

#ifdef __cplusplus
}
#endif

#endif /* __COMMUNICATION_H */
