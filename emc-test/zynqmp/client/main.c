/******************************************************************************/
/**
 *
 * @file main.c
 * Author: alen.arias.vazquez@cern.ch
 *
 ******************************************************************************/

/***************************** Include Files **********************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <gpiod.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <sensors/sensors.h>

#include "diot_util_sensors.h"
#include "diot_util_gpio.h"
#include "diot_util_error.h"
#include "communication.h"
#include "sys_util.h"
#include "fantray.h"

/************************** Constant Definitions ******************************/
/**************************** Type Definitions ********************************/
/***************** Macros (Inline Functions) Definitions **********************/
/************************** Variable Definitions ******************************/
/************************** Function Prototypes *******************************/
void print_help(char *name);
void sigint_handler();
void signal_service_stop();
void set_up_service();

/******************************************************************************/
/**
 * @Brief Main
 *
 ******************************************************************************/
int main(int argc, char **argv) {

    int port = 0;
    int opt = 0;
    char * SERVER_IP = NULL;

    if (argc != 5) {
        print_help(argv[0]);
        exit(0);
    }

    while((opt = getopt(argc, argv, "a:p:")) != -1) {
        switch(opt) {
            case 'a':
#if DEBUG == 1
                printf("Server IP address: %s\n", optarg);
#endif
                SERVER_IP = (char *) calloc (strlen(optarg), sizeof(char));
                memcpy(SERVER_IP, optarg, strlen(optarg));
                break;

            case 'p':
#if DEBUG == 1
                printf("Server Port: %s\n", optarg);
#endif
                port = atoi(optarg);
                if ((port == 0) || (port < 1024)) {
                    printf("Port has to be a numero greather than 1024\n");
                    exit(0);
                }
                break;

            case '?':
                printf("unknown option: %c\n", optopt);
                print_help(argv[0]);
                break;

        }
    }

    //! Variables
    int len = 0;
    uint32_t pkg_number= 0;
    size_t data_send = 0;
    diot_login_s * tx_frame;

    //! Signals
    signal(SIGINT, sigint_handler);
    signal(SIGTERM, signal_service_stop);

    // Set up connection
    int sock_id = open_socket();
    struct sockaddr_in server_addr;
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    server_addr.sin_port = htons(port);

    //! Set up service
    set_up_service();

    // try to connect the client socket to server socket
    connect_to_server(sock_id, server_addr);
    fantray_soft_reset();
    sleep(1.0);

    while(1) {

        tx_frame = (diot_login_s *) calloc (1, sizeof(diot_login_s));

        if (! tx_frame) {

            printf("Reserved memory failure\n");
            break;

        } else {

            read_all_sensors();
            fill_header(tx_frame, pkg_number);
            len = get_sensor_data(tx_frame, 5);
            len = len + get_memory_info(tx_frame, len+5);
            tx_frame->buffer[4] = len;

            // Calculate CR
            tx_frame->buffer[C_MAX_BUFFER_SIZE_W-1] = singletable_crc32c(C_CRC_INIT, tx_frame->buffer, len*4);

            // Send
            data_send = send(sock_id, tx_frame, sizeof(diot_login_s), 0);
            if (data_send != sizeof(diot_login_s)) {
                printf("\n");
                printf("PKG NUM               : %u\n",tx_frame->buffer[1]);
                printf("Size of struct        : %lu\n",sizeof(diot_login_s));
                printf("Data send             : %lu\n",data_send);
                printf("Error sending data\n");
                break;
            }

            data_send = 0;
            pkg_number++;
            free(tx_frame);

        }

#if DEBUG == 1
        printf("\n");
        printf("pkg_number %u\n",pkg_number);
        printf("\n");
        sleep(2.0);
#else
        sleep(C_LOG_PERIOD);
#endif


    }

    shutdown(sock_id, SHUT_WR);
    shutdown(sock_id, SHUT_RD);
    close(sock_id);
    sensors_cleanup();
    close_gpios();

    return 0;

}

/***************************** Function Bodys *********************************/
/******************************************************************************/
/**
 * @Brief Print Help
 *
 ******************************************************************************/
void print_help(char *name) {

    printf("Usage: \n");
    printf("        ./%s -a <ip_address> -p <port>\n",name);
    printf("        ./%s -a 192.168.0.2 -p 3333\n",name);
    printf("\n");

}

/******************************************************************************/
/**
 * @Brief Set up the enviromment
 *
 ******************************************************************************/
void set_up_service() {

    open_gpios();
    sensors_init(NULL);
    diot_error_clear();
    psu_added_or_removed(1);
    psu_added_or_removed(2);

}

/******************************************************************************/
/**
 * @Brief Handler in case ctrl+C, used only for test
 *
 ******************************************************************************/
void sigint_handler() {

    printf("killing process %d\n",getpid());
    sensors_cleanup();
    close_gpios();
    exit(0);

}

/******************************************************************************/
/**
 * @Brief Handler systemctrl stop
 *
 ******************************************************************************/
void signal_service_stop() {

    printf("Service with PID %d\n stopped",getpid());
    sensors_cleanup();
    close_gpios();
    exit(0);

}
