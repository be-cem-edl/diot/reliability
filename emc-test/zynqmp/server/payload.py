"""

    Test Payload
    Author: alen.arias.vazquez@cern.ch

"""

EMC_ZU_PAYLOAD = {
    0  : 'Temperature FPGA',
    1  : 'Temperature FMC',
    2  : 'Temperature DDR',
    3  : 'Temperature Power Management',
    4  : 'IRPS1 P0V85 Temperature',
    5  : 'IRPS1 P1V8 AUX Temperature',
    6  : 'IRPS1 P1V2 Temperature',
    7  : 'IRPS1 P1V8 Temperature',
    8  : 'IRPS1 P1V2 PSPLL LDO Temperature',
    9  : 'IRPS2 P3V3 Temperature',
    10 : 'IRPS2 MGT 1V8 Temperature',
    11 : 'IRPS2 MGT 0V9 Temperature',
    12 : 'IRPS2 MGT 1V2 Temperature',
    13 : 'PSU1 Temperature 1',
    14 : 'PSU1 Temperature 2',
    15 : 'PSU2 Temperature 1',
    16 : 'PSU2 Temperature 2',
    17 : 'Input Voltage',
    18 : 'Input Current',
    19 : 'P0V85 Voltage',
    20 : 'P0V85 Current',
    21 : 'P1V8 AUX Voltage',
    22 : 'P1V8 AUX Current',
    23 : 'P1V2 Voltage',
    24 : 'P1V2 Current',
    25 : 'P1V8 Voltage',
    26 : 'P1V8 Current',
    27 : 'P1V2_PSPLL LDO Voltage',
    28 : 'P1V2_PSPLL LDO',
    29 : 'Input Voltage',
    30 : 'Input Current',
    31 : 'P3V3 Voltage',
    32 : 'P3V3 Current',
    33 : 'MGT 1V8 Voltage',
    34 : 'MGT 1V8 Current',
    35 : 'MGT 0V9 Voltage',
    36 : 'MGT 0V9 Current',
    37 : 'MGT 1V2 Voltage',
    38 : 'MGT 1V2 Current',
    39 : 'PSU1 Voltage',
    40 : 'PSU1 Current',
    41 : 'PSU2 Voltage',
    42 : 'PSU2 Current',
    43 : 'Uptime',
    44 : 'Load 1m',
    45 : 'Load 5m',
    46 : 'Load 15m',
    47 : 'Total Ram',
    48 : 'Free Ram',
}

EMC_ZU_PAYLOAD_LEN = 49
