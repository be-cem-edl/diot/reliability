"""

    Basic TCP server
    Author: alen.arias.vazquez@cern.ch

"""

import socket
import sys
import serial
import logging, logging.config
import argparse
import numpy as np
import json
import string

from crc import CrcCalculator, Configuration
from datetime import datetime, timezone
from influx import BasicInflux

class BasicServer(object):

    MAX_SIZE_B = 1024
    HEADER = 0x454D4354

    crc_width = 32
    crc_poly = 0x1EDC6F41
    crc_init_value = 0x0
    crc_final_xor_value = 0x0
    crc_reverse_input = True
    crc_reverse_output = True

    def __init__(self, name='Server', port= 30000, log_file='',log_level=logging.DEBUG):
        if log_file == '':
            logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)s-%(levelname)-s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        else:
            logging.basicConfig(filename=log_file, format='%(asctime)s.%(msecs)03d : %(levelname)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        self.name = name
        self.log = logging.getLogger(self.name)
        self.ip = self.get_ip_address()
        self.port = port
        self.sockfd = None
        self.connection = None
        self.client_ip = None
        self.expected_pkg = 0
        self.err_pkg_num = 0
        self.err_pkg_header = 0
        self.err_crc = 0
        self.crc_configuration = Configuration(self.crc_width, self.crc_poly, self.crc_init_value, self.crc_final_xor_value, self.crc_reverse_input, self.crc_reverse_output)
        self.crc_calculator = CrcCalculator(self.crc_configuration)
        self.DB = BasicInflux(name=self.name+'-Influx', log_file='',log_level=log_level)
        self.connectionDB = self.DB.influxDBconnect(password = 'diot12345')
        self.timezone = timezone.utc

    def get_ip_address(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(("8.8.8.8", 80))
            return s.getsockname()[0]
        except socket.error:
            logging.error("Caught exception socket.error : {}".format(socket.error))
            return "localhost"

    def bind_socket(self):
        self.sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sockfd.bind((self.ip, self.port))
        self.sockfd.listen(1)
        self.log.info("Waiting for a connection")

    def connect_accept(self):
        self.connection, self.client_ip = self.sockfd.accept()
        self.log.info("Connection accepted from {}".format(self.client_ip))
        self.connection.settimeout(5.0)

    def set_blocking(self):
        self.log.info("Set socket as blocking")
        self.sockfd.setblocking(True)

    def set_nonblocking(self):
        self.log.info("Set socket as non blocking")
        self.sockfd.setblocking(False)

    def close_socket(self):
        if self.sockfd != None:
            self.sockfd.close()
        self.log.info("Socket closed")

    def close_connect(self):
        if self.connection != None:
            self.connection.close()
        self.log.info("Connection closed")

    def convert_time_float(self, time_s, time_ns):
        a = float(time_ns) / 1000000000
        return (a + float(time_s))

    def recv_data(self, data_len=256):

        self.log.debug("Reception data")
        rx_done = False
        len_rx = 0
        len_exp = data_len
        data = bytes()
        buffer = bytes()

        while rx_done != True:

            data = self.connection.recv(len_exp)
            len_rx = len_rx+len(data)

            if len_rx < len_exp:
                len_exp = len_exp - len_rx
                len_rx = 0
            elif len_rx == len_exp:
                len_exp = len_exp - len_rx
                len_rx = 0
                rx_done = True
            else:
                self.log.error("This case should never happen")
            buffer += data

        return buffer

    def process_data_rx(self,byte_array):
        if len(byte_array) != self.MAX_SIZE_B:
            self.log.error("Error in length received: {}".format(len(byte_array)))
            exit()

        buffer = np.frombuffer(byte_array, dtype=np.uint32)
        crc_expected = self.crc_calculator.calculate_checksum(byte_array[0:buffer[4]*4])
        if buffer[-1] != crc_expected:
            self.log.error("CRC {} --- CRC Expected {}".format(hex(buffer[-1]),hex(crc_expected)))
            self.err_crc += 1
            return

        if buffer[0] != self.HEADER:
            self.log.error("Invalid Header, package discarted")
            self.err_pkg_num += 1
            return

        else:
            if buffer[1] != self.expected_pkg:
                self.err_pkg_num += 1
                self.log.error("Package expected {} --- Package received {}".format(self.expected_pkg,buffer[1]))
                if buffer[1] > self.expected_pkg:
                    self.expected_pkg = buffer[1]+1
            else:
                self.log.debug("Package number {}".format(buffer[1]))
                self.expected_pkg += 1
            self.log.debug("Timestamp {}.{}".format(buffer[2],buffer[3]))
            self.log.debug("Payload length {}".format(buffer[4]))
            self.log.debug("Payload: [ {}, ..., {} ]".format(buffer[5],buffer[5+buffer[4]]))
            timestamp_float = self.convert_time_float(buffer[2],buffer[3])
            self.write_data_db(timestamp_float=timestamp_float, data=buffer[5:5+buffer[4]])
            return

    def write_data_db(self, timestamp_float, data):

        self.log.debug("DATA LEN for DB {}".format(len(data)))
        timestamp_str = datetime.fromtimestamp(timestamp_float, self.timezone).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

        DataLogged = [
            {
                "measurement"   : "EMC",
                "tags": {
                    "device"    : "ZynqMP",
                    "location"  : "CERN"
                },
                "time": timestamp_str,
                "fields": {
                    "Temperature FPGA"                  : data[0]  / 1000.0,
                    "Temperature FMC"                   : data[1]  / 1000.0,
                    "Temperature DDR"                   : data[2]  / 1000.0,
                    "Temperature Power Management"      : data[3]  / 1000.0,
                    "IRPS1 P0V85 Temperature"           : data[4]  / 1000.0,
                    "IRPS1 P1V8 AUX Temperature"        : data[5]  / 1000.0,
                    "IRPS1 P1V2 Temperature"            : data[6]  / 1000.0,
                    "IRPS1 P1V8 Temperature"            : data[7]  / 1000.0,
                    "IRPS1 P1V2 PSPLL LDO Temperature"  : data[8]  / 1000.0,
                    "IRPS2 P3V3 Temperature"            : data[9]  / 1000.0,
                    "IRPS2 MGT 1V8 Temperature"         : data[10] / 1000.0,
                    "IRPS2 MGT 0V9 Temperature"         : data[11] / 1000.0,
                    "IRPS2 MGT 1V2 Temperature"         : data[12] / 1000.0,
                    "PSU1 Temperature 1"                : data[13] / 1000.0,
                    "PSU1 Temperature 2"                : data[14] / 1000.0,
                    "PSU2 Temperature 1"                : data[15] / 1000.0,
                    "PSU2 Temperature 2"                : data[16] / 1000.0,
                    "IRPS1 Input Voltage"               : data[17] / 1000.0,
                    "IRPS1 Input Current"               : data[18] / 1000.0,
                    "P0V85 Voltage"                     : data[19] / 1000.0,
                    "P0V85 Current"                     : data[20] / 1000.0,
                    "P1V8 AUX Voltage"                  : data[21] / 1000.0,
                    "P1V8 AUX Current"                  : data[22] / 1000.0,
                    "P1V2 Voltage"                      : data[23] / 1000.0,
                    "P1V2 Current"                      : data[24] / 1000.0,
                    "P1V8 Voltage"                      : data[25] / 1000.0,
                    "P1V8 Current"                      : data[26] / 1000.0,
                    "P1V2_PSPLL LDO Voltage"            : data[27] / 1000.0,
                    "P1V2_PSPLL LDO"                    : data[28] / 1000.0,
                    "IRPS2 Input Voltage"               : data[29] / 1000.0,
                    "IRPS2 Input Current"               : data[30] / 1000.0,
                    "P3V3 Voltage"                      : data[31] / 1000.0,
                    "P3V3 Current"                      : data[32] / 1000.0,
                    "MGT 1V8 Voltage"                   : data[33] / 1000.0,
                    "MGT 1V8 Current"                   : data[34] / 1000.0,
                    "MGT 0V9 Voltage"                   : data[35] / 1000.0,
                    "MGT 0V9 Current"                   : data[36] / 1000.0,
                    "MGT 1V2 Voltage"                   : data[37] / 1000.0,
                    "MGT 1V2 Current"                   : data[38] / 1000.0,
                    "PSU1 Voltage"                      : data[39] / 1000.0,
                    "PSU1 Current"                      : data[40] / 1000.0,
                    "PSU2 Voltage"                      : data[41] / 1000.0,
                    "PSU2 Current"                      : data[42] / 1000.0,
                    "FANTRAY 12V"                       : data[43] / 1000.0,
                    "FANTRAY 5V"                        : data[44] / 1000.0,
                    "FAN1 Current"                      : data[45] / 1000.0,
                    "FAN2 Current"                      : data[46] / 1000.0,
                    "FAN3 Current"                      : data[47] / 1000.0,
                    "FAN1 RPM"                          : data[48] / 1000.0,
                    "FAN2 RPM"                          : data[49] / 1000.0,
                    "FAN3 RPM"                          : data[50] / 1000.0,
                    "Uptime"                            : data[51],
                    "Load 1m"                           : (data[52] / 65536.0)*100,
                    "Load 5m"                           : (data[53] / 65536.0)*100,
                    "Load 15m"                          : (data[54] / 65536.0)*100,
                    "Total Memory"                      : data[55],
                    "Free Memory"                       : data[56],
                    "% Used Memory"                     : ((data[55]-data[56])/data[55])*100.0,
                    "CRC Errors"                        : self.err_crc,
                    "PKG Errors"                        : self.err_pkg_num
                }
            }
        ]


        success = self.DB.influxDBwrite(influxDBConnection=self.connectionDB, DataLogged=DataLogged)
        if success:
            self.log.debug("Data pushed in the DB")
        else:
            self.log.error("Data not pushed in the DB")

def main():

    parser = argparse.ArgumentParser(description='Basic Web Application')
    parser.add_argument('--port', '-p'  , type=int,  default= 15666, help='Port for the server')
    parser.add_argument('--debug','-dbg', type=bool, default= False, help='Activates debug mode')
    args = parser.parse_args()
    ServerPort = args.port
    ServerDebugMode = args.debug

    try:
        if ServerDebugMode:
            log_level=logging.DEBUG
        else:
            log_level=logging.INFO

        my_server = BasicServer(name='Server', log_file='', port=ServerPort, log_level=log_level)
        my_server.bind_socket()

        while True:

            # Wait for a connection
            my_server.connect_accept()

            # Set Socket as blocking
            my_server.set_nonblocking()

            try:

                while True:
                    my_server.log.debug("Expecting PKG number: {}".format(my_server.expected_pkg))
                    buffer = my_server.recv_data(my_server.MAX_SIZE_B)
                    my_server.process_data_rx(buffer)

            except (TypeError, socket.error, socket.timeout) as msg:
                my_server.log.error("Socket Error: {}".format(msg))
                my_server.close_connect()
                my_server.close_socket()
                my_server.DB.influxDBclose(influxDBConnection=my_server.connectionDB)
                break

    except KeyboardInterrupt:
        my_server.log.error("Server stopped by the user")
        my_server.close_connect()
        my_server.close_socket()
        my_server.DB.influxDBclose(influxDBConnection=my_server.connectionDB)

if __name__ == '__main__':
    sys.exit(main())
