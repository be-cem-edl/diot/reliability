"""

    Basic Script to push data into an influx DB
    Author: alen.arias.vazquez@cern.ch

"""

import json
import requests
import os
import glob
import time
import logging, logging.config

from influxdb import InfluxDBClient

class BasicInflux(object):

    def __init__(self, name='Influx', log_file='',log_level=logging.DEBUG):
        if log_file == '':
            logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)s-%(levelname)-s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        else:
            logging.basicConfig(filename=log_file, format='%(asctime)s.%(msecs)03d : %(levelname)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=log_level)
        self.name = name
        self.log = logging.getLogger(self.name)
        self.influxDBHost = 'dbod-diot-zu-emc-test.cern.ch'
        self.influxDBPort = 8080
        self.influxDBUser = 'admin'
        self.influxDBName = 'diot_zu_emc'

    def influxDBconnect(self, password):
        self.log.info("Connecting to database {} at the host {}".format(self.influxDBName, self.influxDBHost))
        return InfluxDBClient(host=self.influxDBHost, port=self.influxDBPort, username=self.influxDBUser, password=password, database=self.influxDBName, ssl=True, verify_ssl=False, retries=0)

    def influxDBwrite(self, influxDBConnection, DataLogged):
        try:
            return influxDBConnection.write_points(DataLogged, time_precision='ms')
        except ConnectionError as msg:
            self.log.error("Error Writing: {}".format(msg))
            return False


    def influxDBclose(self, influxDBConnection):
        self.log.info("Closing connection with host {}".format(self.influxDBHost))
        influxDBConnection.close()

    def influxDBping(self, influxDBConnection, n):
        return influxDBConnection.ping()
